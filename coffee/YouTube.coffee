youtube_embed_format = (src, width, height) ->
	first_part = "<iframe title='YouTube video player' class='youtube-player' type='text/html' "
	second_part = "width='" + width + "' height='" + height + "' "
	third_part = "src='http://www.youtube.com/embed/" + src + "' frameborder='0' allowFullScreen></iframe>"

	first_part + second_part + third_part

window.youtube_do = (video, width, height) ->
	regexp_url = /((mailto\:|(news|(ht|f)tp(s?))\:\/\/){1}\S+)/

	if regexp_url.test(video) == false
		document.writeln youtube_embed_format(youtube_id_extract(video), width, height)
	else
		document.writeln youtube_embed_format(video, width, height)

youtube_id_extract = (url) ->
	url.replace(/^[^v]+v.(.{11}).*/,"$1")