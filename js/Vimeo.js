// Vimeo.js
// Vimeo video embedding script

function vimeo_embed_format(src, width, height) {
  var final_syntax;
  var first_part;
  var second_part;
  var third_part;
  
  first_part = "<iframe src='http://player.vimeo.com/video/" + src + "' ";
  second_part = "width='" + width + "' height='" + height +"' ";
  third_part = "frameborder='0'></iframe>";
  final_part = first_part + second_part + third_part;
  
  return final_part;
}

function vimeo_do(video, width, height) {
  var make_syntax;
  var regexp_url;

  regexp_url = /((mailto\:|(news|(ht|f)tp(s?))\:\/\/){1}\S+)/;

  if(regexp_url.test(video) == false) {
    vimeo_id = vimeo_id_extract(video)

    if(vimeo_id != -1) {
      make_syntax = vimeo_embed_format(vimeo_id, width, height);
    }
    else {
      // there is no video id in the string passed in
    }
  } else {
    make_syntax = vimeo_embed_format(video, width, height);
  }

  document.writeln(make_syntax);
}

function vimeo_id_extract(url) {
  var re;
  var match;

  re = new RegExp('[0-9]{8}');
  match = re.exec(url);

  if(match != null) {
    return match[0];
  }
  else {
    return -1;
  }
}