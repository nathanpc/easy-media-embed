// HTML5Video.js
// HTML5 Video embedding script

// THIS WON'T WORK AT THE TIME!!!!!!!!!

function htmlvideo_embed_format(src_mp, src_ogg, src_webm, width, height, controls) {
  var p_vdtag;
  var mptag;
  var oggtag;
  var wmtag;
  var dcontrols;
  var _width;
  var _height;
  var fvTag;
  var fullvTag;
  
  if (width = null) {
    _width = "320"; // Default width
  } else {
    _width = width; // Use the provided one
  }
  
  if (height = null) {
    _height = "240"; // No height provided so use the default
  } else {
    _height = height; // You already know this :P
  }
  
  if (controls = true) {
    dcontrols = "controls "; // So use them
  } else {
    dcontrols = ""; // No, I don't want this
  }
  
  p_vdtag = "<video width='" + _width + "' height='" + _height + "' " + dcontrols + ">";
  fvTag = "</video>";
  
  if (src_mp != null) {
    mptag = "<source src='" + src_mp + "' type='video/mp4' />";
  } else {
    mptag = "";
  }
  
  if (src_ogg != null) {
    oggtag = "<source src='" + src_ogg + "' type='video/ogg' />";
  } else {
    oggtag = "";
  }
  
  if (src_webm != null) {
    wmtag = "<source src='" + src_webm + "' type='video/webm' />";
  } else {
    wmtag = "";
  }
  
  fullvTag = p_vdtag + "\n" + mptag + "\n" + oggtag + "\n" + wmtag + "\n" + fvTag;
  document.write(fullvTag);
}