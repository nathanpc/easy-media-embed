// YouTube.js
// YouTube video embedding script

function youtube_embed_format(src, width, height) {
  var final_syntax;
  var first_part;
  var second_part;
  var third_part;
  
  first_part = "<iframe title='YouTube video player' class='youtube-player' type='text/html' ";
  second_part = "width='" + width + "' height='" + height +"' ";
  third_part = "src='http://www.youtube.com/embed/" + src + "' frameborder='0' allowFullScreen></iframe>";
  final_part = first_part + second_part + third_part;
  
  return final_part;
}

function youtube_do(video, width, height) {
  var make_syntax;
  var regexp_url;
  
  regexp_url = /((mailto\:|(news|(ht|f)tp(s?))\:\/\/){1}\S+)/;
  
  if(regexp_url.test(video) == false) {
    make_syntax = youtube_embed_format(youtube_id_extract(video), width, height);
  } else {
    make_syntax = youtube_embed_format(video, width, height);
  }
  
  document.writeln(make_syntax);
}

function youtube_id_extract(url) {
  var youtube_id;
  youtube_id = url.replace(/^[^v]+v.(.{11}).*/,"$1"); //url.match(/\?v=(.*)/)[1];

  return youtube_id;
}